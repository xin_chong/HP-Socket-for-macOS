#!/bin/zsh
dir=$1
des=$2

read_dir(){
if [ -f $dir ]
then
    otool -L $dir | awk  '{if(start=match($1,"/")){ if(start==1){printf("%s\n"),$1 }} }' | xargs -I F sh -c "cp -f F $des"
    return
fi
for file in `ls $dir`
do
    filepath="$dir/$file"
    if [ -d $filepath ]
    then
        # dir
        echo $filepaht
    else
        otool -L $filepath | awk  '{if(start=match($1,"/")){ if(start==1){printf("%s\n"),$1 }} }' | xargs -I F sh -c "cp F $des"
    fi
done
}
read_dir
